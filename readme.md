# Quick-start

* Add the relevant *.a library file
* Include sLED_driver.h and sLED_functions.h
* `#define` `SLED_DRIVER_RGB`, `SLED_DRIVER_RGBW` or `SLED_DRIVER_WWA` in your project
* Declare the following
..```
single_led_t your_complete_led_array[NUMBER_OF_LEDS];
led_config_t your_led_config = {
        .array_ptr = your_led_array,
        .array_columns = NUMBER_OF_COLUMNS,
        .array_rows = NUMBER_OF_ROWS,
        .arrays = NUMBER_OF_ARRAYS,
        .color_mask = SLED_MASK_RGB,
        .input_color = {
            .red = 0xff,
        }
};```
* Configure the AVR to have clock settings corresponding to what the library needs
* Init the driver with `sLED_driver_init()`
* Update the entire array to the `.input_color` settings with `sLED_array_update_all(&your_led_config)` 
* Push array data to LEDs with `sLED_configure_all(&your_led_config)`
* Your LED-strip/array should now light up in red.

# Pinout

At the moment the driver has a certain data out pin for each device:
* ATmega324PB: PD5
* Xmega E5: PC0
* Xmega A4: PD0
* ATtiny817: PC1 (And using PA1, PA3 and PD2 as well?)