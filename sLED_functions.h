/*
 * sLED_driver.h
 *
 * Created: 08.09.2016 09:20:49
 * Author : Egil Rotevatn
 */ 

#ifndef _sLED_functions_H_
#define _sLED_functions_H_

#include "sLED_driver.h"

typedef struct led_config {
	single_led_t *array_ptr;	// Pointer to LED array(s)
	uint8_t arrays;				// Number of physical arrays
	uint8_t array_rows;			// Number of rows in array(s)
	uint8_t array_columns;		// Number of columns in array(s)
	uint8_t color_mask;			// Color channel(s) that can be configured
	single_led_t input_color;	// Input colors, if relevant
} led_config_t;

/*********************************************************************************
Write functions for updating contents of a data struct array
*********************************************************************************/
void sLED_array_update_pixel(led_config_t *led, uint8_t pixel);
void sLED_array_clear_pixel(led_config_t *led, uint8_t pixel);
bool sLED_array_fade_pixel(led_config_t *led, uint8_t pixel);
bool sLED_array_fade_off_pixel(led_config_t *led, uint8_t pixel);

void sLED_array_update_all(led_config_t *led);
void sLED_array_clear_all(led_config_t *led);
bool sLED_array_fade_all(led_config_t *led);
bool sLED_array_fade_off_all(led_config_t *led);

void sLED_array_clear_all_set_pixel(led_config_t *led, uint8_t pixel);
void sLED_array_update_pixel_range(led_config_t *led, uint8_t startpixel, uint8_t pixels);
void sLED_array_update_pixel_row(led_config_t *led, uint8_t array, uint8_t row);
void sLED_array_update_pixel_column(led_config_t *led, uint8_t array, uint8_t column);

void sLED_array_update_pixel_diagonal_rising(led_config_t *led, uint8_t array, uint8_t diagonal);
void sLED_array_update_pixel_diagonal_falling(led_config_t *led, uint8_t array, uint8_t diagonal);

void sLED_configure_all(led_config_t *led);
void sLED_configure_all_oddflip(led_config_t *led);

#endif